var myApp = angular.module('Carros',[]);

myApp.controller('CarrosController', ['$scope', function($scope) {
  	$scope.listaDeCarros = 'Lista De Carros';
  	$scope.adicionar = 'Adicionar';
  	$scope.incluir = "Incluir";
  	$scope.fechar = "Fechar [X]";
  	$scope.carro = "Carro";
  	$scope.ano = "Ano";
    $scope.id = "Id";
  	$scope.quilometragem = "Km";
  	$scope.valor = "Valor";
  	$scope.carros = [];
  	$scope.adicionando = false;
    $scope.deleting = false;


  	$scope.adicionarFunction = function(){
  		var carro = $("#carro").val();
		var divErro = $("#errorMessage");

  		if (carro === ""){
  			divErro.text("Carro tem que ser digitado");
  			return
  		}

	    var ano = $("#ano").val();
	    if (ano === ""){
  			divErro.text("Ano tem que ser digitado");
  			return
  		}

	    var km = $("#km").val();
	    if (km === ""){
  			divErro.text("km tem que ser digitado");
  			return
  		}
	    var valor = $("#valor").val();
	    if (valor === ""){
  			divErro.text("Valor tem que ser digitado");
  			return
  		}

    	var data = $("form").serialize();
        $.ajax({
            type : 'POST',
            url  : 'http://vitorgv.com.br/php/criarcontaPHP.php',
            data : data,
            success :  function(data){
               var json = jQuery.parseJSON(data);
               if (json.id){
               		$("#tabela").append("<tr>"+
                            "<td>" + json.id + "</td>" +
           									"<td id='carro'>" + carro + "</td>" +
           									"<td id='ano'>" + ano + "</td>"+
           									"<td id='km'>" + km + "</td>"+
           									"<td id='valor'>" + valor +"</td>" +
       										"<td> <input type='button' class='btn-info' value='Editar'/> </td>" +
                                			"<td> <input type='button' class='btn-danger' value='Excluir' ng-click='delete();'/> </td>" +
           								 "</th>" );
                    $("#fechar").click();
               }
            }, error: function (request, error) {
               console.log(arguments);
               alert(" Can't do because: " + error);
            }
        });
        
  	}

  	$scope.getCarros = function(){
       $.ajax({
            type : 'POST',
            url  : 'http://vitorgv.com.br/php/getAllCars.php',
            success :  function(data){
              var json = jQuery.parseJSON(data);
               for (i = 0; i < json.length; i++){
                  $("#tabela").append("<tr class='remover'>" +
                                        "<td>" + json[i].ID + "</td>" + 
                                        "<td id='carro'>" + json[i].CARRO + "</td>" +
                                        "<td id='ano'>" + json[i].ANO + "</td>" +
                                        "<td id='km'>" + json[i].KM +"</td>" +
                                        "<td id='valor'>" + json[i].VALOR + "</td>" +
                                        "<td> <input type='button' class='btn-info' value='Editar'/> </td>" +
                                        "<td> <input type='button' name='id' id=' " + json[i].ID + "' class='btn-danger' value='Excluir' ng-click='delete();'/> </td>" +
                                      "</tr>");
               }
               console.log(data);
            }, error: function (request, error) {
               console.log(arguments);
               alert(" Can't do because: " + error);
            }
        });
  	}

		$("#tabela").on("click", ".btn-danger", function(e){
        var id = $(this).attr('id');
        $scope.deleting = true;
        $.ajax({
            type : 'POST',
            url  : 'http://vitorgv.com.br/php/deleteCar.php',
            data :  {'id' : id},
            success :  function(data){
            }, error: function (request, error) {
               console.log(arguments);
               alert(" Can't do because: " + error);
            }
        });
		    $(this).closest('tr').remove(); 
		});

    $("#tabela").on("click", "tr", function(e){
        if ($scope.deleting){
          $scope.deleting = false;
          return;
        }
        $("#adicionar").click();
        var formInputs = $('#form :input');
        formInputs[0].value = $(this).find('td')[1].innerHTML;
        formInputs[1].value = $(this).find('td')[2].innerHTML;
        formInputs[2].value = $(this).find('td')[3].innerHTML;
        formInputs[3].value = $(this).find('td')[4].innerHTML;
        /*
        var id = $(this).attr('id');
        $.ajax({
            type : 'POST',
            url  : 'http://vitorgv.com.br/php/.php',
            data :  {'id' : id},
            success :  function(data){
            }, error: function (request, error) {
               console.log(arguments);
               alert(" Can't do because: " + error);
            }
        });
        $(this).closest('tr').remove(); */
    });
}]);