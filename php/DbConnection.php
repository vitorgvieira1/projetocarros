<?php
    function getInstance() {
        $conecta = mysql_connect("50.116.86.94", "vitor632_root", "guizo123", "vitor632_wis");
        return $conecta;
    }

    function executeQuery($sql, $conexao){
        $result = mysql_query($sql, $conexao);
        return $result;
    }

    function conectaPDO(){
        $pdo = new PDO('mysql:host=50.116.86.94;dbname=vitor632_wis', 'vitor632_root', 'guizo123');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    function getPassword($userEmail, $pdo){
        $consulta = $pdo->prepare("SELECT * FROM Usuario where email = :email;");
        $consulta->bindParam(':email', $userEmail, PDO::PARAM_STR);
        $consulta->execute();
        $linha = $consulta->fetch(PDO::FETCH_ASSOC);
        return $linha['Password'];
    }

    function getMaxIdUser($pdo){
        $consulta = $pdo->query("SELECT MAX(ID) as ID FROM Usuario");
        $linha = $consulta->fetch(PDO::FETCH_ASSOC);
        return $linha['ID'] + 1;
    }

    function getMaxIdCarros($pdo){
        $consulta = $pdo->query("SELECT MAX(ID) as ID FROM Carros");
        $linha = $consulta->fetch(PDO::FETCH_ASSOC);
        return $linha['ID'] + 1;
    }

    function createCar($carro, $ano, $km, $valor, $pdo){
        $id = getMaxIdCarros($pdo);
        $consulta = $pdo->prepare("INSERT INTO Carros(ID, CARRO, ANO, KM, VALOR) VALUES (:id, :carro, :ano, :km, :valor)");
        $consulta->execute(array(':id' => $id, ':carro' => $carro, ':ano' => $ano, ':km' => $km, ':valor' => $valor));
        return "ok";
    }

    function createUser($firstname, $secondname, $email, $password, $pdo){
        $id = getMaxIdUser($pdo);
        $consulta = $pdo->prepare("INSERT INTO Usuario(ID, FIRSTNAME, SECONDNAME, EMAIL, PASSWORD) VALUES (:id, :firstname, :secondname, :email, :password)");
        $consulta->execute(array(':id' => $id, ':firstname' => $firstname, ':secondname' => $secondname, ':email' => $email, ':password' => $password));
        return "ok";
    }
    function getUserByEmail($email, $pdo){
    	$consulta = $pdo->prepare("select *  from Usuario where email = :email");
    	$consulta = $pdo->execute(array(':email' => $email));
    	$linha = $consulta->fetch(PDO::FETCH_ASSOC);
	$user = (object) array('email' => $linha['email'], 'firstname' => $linha['firstname'], 'secondname' => $linha['secondname'], 'password' => $linha['password']);
	return $user;
    }
?>